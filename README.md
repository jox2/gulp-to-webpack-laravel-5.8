- Se necesita  [Node.js](https://nodejs.org/dist/v14.18.0/node-v14.18.0-x64.msi) v14+.

- Crear un archivo con el nombre `webpack.mix.js` en la carpeta raiz y debe tener el siguiente contenido:

```js
const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

```

 - Eliminar `gulpfile.js`

 - Reemplazar el contenido de `package.json` :

```json
{
  "private": true,
  "scripts": {
    "dev": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
    "watch": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack/bin/webpack.js --watch --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
    "watch-poll": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack/bin/webpack.js --watch --watch-poll --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
    "hot": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --config=node_modules/laravel-mix/setup/webpack.config.js",
    "production": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=production node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js"
  },
  "devDependencies": {
    "bootstrap-sass": "^3.3.7",
    "cross-env": "^3.2.4",
    "extract-text-webpack-plugin": "^4.0.0-beta.0",
    "jquery": "^1.11.1",
    "laravel-echo": "^1.10.0",
    "laravel-elixir": "^6.0.0-14",
    "laravel-elixir-vue-2": "^0.2.0",
    "laravel-elixir-webpack-official": "^1.0.2",
    "laravel-mix": "^0.10.0",
    "lodash": "^4.16.2",
    "pusher-js": "^7.0.2",
    "socket.io-client": "^3.0.4",
    "vue": "^2.0.1",
    "vue-resource": "^1.0.3",
    "webpack": "^2.2.0",
    "webpack-cli": "^3.1.2",
    "webpack-dev-server": "^4.3.0"
  },
  "dependencies": {}
}


```

 - Eliminar `require('vue-resource');` en el archivo `resources/assets/js/bootstrap.js`

 - Reemplazar:
 
```js
Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    next();
});
```

Por:

```js
window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};
```
 
 - En la plantilla principal agregar `https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js` (para ie9>>)

 - Run `yarn` (or `npm install`)
 
 - Run `npm run watch`to start working.
 
## Comprobar vue2 en nuestro proyecto laravel 5.8

>  Esto no causara problemas en nuestro proyecto original

Para esto debera estar corriendo el siguiente comando `npm run watch`  dentro de su proyecto 

 - Dentro de public/js debera integrar el archivo app.js
 - Dentro de public/css  debera integrar el archivo app.css

En routes/web.php debera modificar la siguiente linea

```sh
 Route::get('/', function () {
    return View::make("Sesion/index");
 });
```

a

```sh
 Route::get('/', function () {
    return View::make("layouts/app");
 });
```

Dentro de resources/views/layouts/app.blade.php debera contener lo siguiente.

```sh
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    
    <link href="{{URL::to('/')}}/css/app.css" rel="stylesheet">
    <script src="{{ asset('js/polyfill.min.js') }}"></script>
   
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                </div>
            </div>
        </nav>

        <example></example>
    </div>

    <!-- Scripts -->
    <script src="{{URL::to('/')}}/js/app.js"></script>
</body>
</html>

```
 